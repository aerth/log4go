package main

import (
	"io"
	"os"

	"gitlab.com/aerth/log4go"
)

func main() {
	log := log4go.New("")
	tmplog := "/tmp/tmplog.tmp.log"
	log.AddFilter("file", log4go.TRACE, log4go.NewFileWriter(tmplog, log4go.FormatDefault))
	log.Info("Autodetected color?")
	log.Color(true)
	log.Warn("Color is now on for sure")
	log.Sync()
	log.Color(false)
	log.Warn("Color is now off for this message")
	log.Sync()
	log.Color(true)
	log.Info("Color is back on for this message")
	log.Debug("Will now replay debug log file, should be plain no color no escapes:")
	log.Sync()
	log.Sync()
	log.Close()
	f, err := os.Open(tmplog)
	if err != nil {
		panic(err)
	}
	io.Copy(os.Stdout, f)
	f.Close()
	os.Remove(tmplog)
	println("Any color or escape codes in above chunk?")
	return
}

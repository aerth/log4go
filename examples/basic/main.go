package main

import (
	"os"
	"sync"

	"gitlab.com/aerth/log4go"
)

func main() {
	var log = log4go.New("") // same as log4go.FormatDefault, try FormatDev
	//var log = log4go.New(log4go.FormatDev) // has caller func name and line number

	wg := sync.WaitGroup{}
	for i := 0; i < 20; i++ {
		wg.Add(1)
		go func(x int) {
			log.Info("hello, world: %d", x)
			wg.Done()
		}(i)
	}
	wg.Wait()
	log.Sync() // or Close(). with no file-logger, it is the same.
	os.Exit(0)
}

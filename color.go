package log4go

import (
	"os"

	"github.com/fatih/color"
	"github.com/mattn/go-isatty"
)

// ColorIsOnByDefault would be a constant... don't modify.
var ColorIsOnByDefault = !(os.Getenv("TERM") == "dumb" ||
	(!isatty.IsTerminal(os.Stdout.Fd()) && !isatty.IsCygwinTerminal(os.Stdout.Fd())))

// LevelStringsColor is the colorized array of level strings.
var LevelStringsColor [10]string = DefaultLevelStringsColor()

// DefaultLevelStringsColor returns the default colorized array of level strings.
// Use this to modify and assign to LevelStringsColor
func DefaultLevelStringsColor() [10]string {
	s := DefaultLevelStrings()
	s = colorize(s)
	return s
}

// Color adds color to log filters that are ConsoleLogWriters
func (l Logger) Color(onoff bool) {
	colorizer(l, onoff)
}

func (f *ConsoleLogWriter) SetColor(onoff bool) {
	f.colorized = onoff
}

func colorizer(l Logger, onoff bool) {
	for wr := range l {
		// only colorize console log writers
		logger, ok := l[wr].LogWriter.(*ConsoleLogWriter)
		if !ok {
			continue
		}
		logger.SetColor(onoff)
	}
}

func colorize(s [10]string) [10]string {
	orange := color.New(color.FgYellow)
	green := color.New(color.FgGreen)
	cyan := color.New(color.FgCyan)
	red := color.New(color.FgRed)
	s[WARNING] = orange.Sprint(s[WARNING])
	s[INFO] = green.Sprint(s[INFO])
	s[DEBUG] = cyan.Sprint(s[DEBUG])
	s[ERROR] = red.Sprint(s[ERROR])
	s[CRITICAL] = red.Sprint(s[CRITICAL])
	return s
}

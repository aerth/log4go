package log4go

import (
	"fmt"
	"os"
)

func recoverPanic() {
	if e := recover(); e != nil {
		fmt.Fprintf(os.Stderr, "⚠️  logger recovered from panic: %v\n", e)
	}
}

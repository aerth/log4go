# aerth/log4go

[fork](https://github.com/alecthomas/log4go).

[fork](https://github.com/jeanphorn/log4go) 

[fork](https://code.google.com/archive/p/log4go/?deadlink)


### Usage

```
package main

import (
        "gitlab.com/aerth/log4go"
)

func main() {
        var log = log4go.New("")
        log.Color(false) // color on by default if using terminal
        for i := 0; i < 20; i++ {
                log.Trace("i is %d", i)
                if i % 2 == 0 {
                log.Info("hello, world: %d", i)
                }
        }
        log.Warn("this is a warning, almost out of..")
        log.Sync()
        log.Critical("this actually wont show up because the program quits, need Sync or Close."
}
```


### Big Warning

Lots of old methods are still in this library and will eventually be removed.

The `log = log4go.New("")` and the `weblog.SupportedLogger` interface is the newer dummyproof stuff.


### example viewing output without timestamp or source line, only level and message

jsonlogview script is provided in ./scripts directory

```
tail -f /path/to/file.log | jsonlogview
```


### weblog

also see the http.Handler wrapper that logs response codes in the weblog directory

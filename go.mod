module gitlab.com/aerth/log4go

go 1.15

require (
	github.com/fatih/color v1.10.0
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-isatty v0.0.12
)

// Copyright (C) 2010, Kyle Lemons <kyle@kylelemons.net>.  All rights reserved.

// Package log4go provides level-based and highly configurable logging to stdio, file, and net
package log4go

import (
	"fmt"
	"os"
	"runtime"
	"strings"
	"time"
)

// Version information (since fork 3.0.1)
const (
	L4G_VERSION = "aerth/log4go-v0.0.11"
	L4G_MAJOR   = 0
	L4G_MINOR   = 0
	L4G_BUILD   = 11
)

/****** Constants ******/

// These are the integer logging levels used by the logger
type Level int

const (
	_ Level = iota
	FINEST
	FINE
	TRACE
	DEBUG
	INFO
	WARNING
	ERROR
	CRITICAL
	UNKNOWN
)

// Logging level strings
var (
	defaultLevelStrings = [10]string{"INVALID", "FNST", "FINE", "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "CRITICAL", "UNKNOWN"}
	LevelStrings        = DefaultLevelStrings()
)

func DefaultLevelStrings() [10]string {
	d := defaultLevelStrings
	s := [10]string{
		d[0], d[1], d[2], d[3],
		d[4], d[5], d[6], d[7],
		d[8], d[9],
	}
	return s
}

func Level2String(l Level) string {
	return l.String()
}

func (l Level) String() string {
	if l < 0 || int(l) > 9 {
		return "UNKNOWN"
	}
	return LevelStrings[int(l)]
}

func (l Level) StringCustom(s [10]string) string {
	if l < 0 || int(l) > 9 {
		return "UNKNOWN"
	}
	return s[int(l)]
}

/****** Variables ******/
var (
	// LogBufferLength specifies how many log messages a particular log4go
	// logger can buffer at a time before writing them.
	LogBufferLength = 32
)

/****** LogRecord ******/

// A LogRecord contains all of the pertinent information for each message
type LogRecord struct {
	Level    Level     // The log level
	Created  time.Time // The time at which the log message was created (nanoseconds)
	Source   string    // The message source
	Message  string    // The log message
	Category string    // The log group
}

/****** LogWriter ******/

// This is an interface for anything that should be able to write logs
type LogWriter interface {
	// This will be called to log a LogRecord message.
	LogWrite(rec *LogRecord)

	// This should clean up anything lingering about the LogWriter, as it is called before
	// the LogWriter is removed.  LogWrite should not be called after Close.
	Close()
}

/****** Logger ******/

// A Filter represents the log level below which no log records are written to
// the associated LogWriter.
type Filter struct {
	Level     Level
	LogWriter LogWriter
	Category  string
}

// LogWrite on the underlying LogWriter
func (f Filter) LogWrite(rec *LogRecord) {
	f.LogWriter.LogWrite(rec)
}

// Close the underlying LogWriter
func (f Filter) Close() {
	f.LogWriter.Close()
}

// A Logger represents a collection of Filters through which log messages are
// written.
type Logger map[string]*Filter

// Create a new logger.
//
// DEPRECATED: Use make(Logger) instead.
func NewLogger() Logger {
	os.Stderr.WriteString("warning: use of deprecated NewLogger\n")
	return make(Logger)
}

// Create a new logger with a DefaultStderrLoggerName filter configured to send log messages at
// or above lvl to standard output.
//
// DEPRECATED: use NewDefaultLogger instead.
func NewConsoleLogger(lvl Level) Logger {
	os.Stderr.WriteString("warning: use of deprecated NewConsoleLogger\n")
	return Logger{
		DefaultStderrLoggerName: &Filter{
			Level:     lvl,
			LogWriter: NewConsoleLogWriter(),
			Category:  "DEFAULT",
		},
	}
}

// Create a new logger with a DefaultStderrLoggerName filter configured to send log messages at
// or above lvl to standard output.
func NewDefaultLogger(lvl Level) Logger {
	return Logger{
		DefaultStderrLoggerName: &Filter{lvl, NewConsoleLogWriter(), "DEFAULT"},
	}
}

// Closes all log writers in preparation for exiting the program or a
// reconfiguration of logging.  Calling this is not really imperative, unless
// you want to guarantee that all log messages are written.  Close removes
// all filters (and thus all LogWriters) from the logger.
func (log Logger) Close() {
	// Close all open loggers
	for name, filt := range log {
		filt.Close()
		delete(log, name)
	}
}

func (log Logger) Sync() {
	type syncer interface {
		Sync()
	}
	for _, v := range log {
		s, ok := v.LogWriter.(syncer)
		if ok {
			s.Sync()
		}
	}
}

// Add a new LogWriter to the Logger which will only log messages at lvl or
// higher.  This function should not be called from multiple goroutines.
// Returns the logger for chaining.
func (log Logger) AddFilter(name string, lvl Level, writer LogWriter, categorys ...string) Logger {
	var c string
	if len(categorys) > 0 {
		c = categorys[0]
	} else {
		c = "DEFAULT"
	}

	log[name] = &Filter{lvl, writer, c}
	return log
}

/******* Logging *******/
// Send a formatted log message internally
func (log Logger) intLogf(lvl Level, format string, args ...interface{}) {
	skip := true

	// Determine if any logging will be done
	for _, filt := range log {
		if lvl >= filt.Level {
			skip = false
			break
		}
	}
	if skip {
		return
	}

	// Determine caller func
	pc, _, lineno, ok := runtime.Caller(2)
	src := ""
	if ok {
		src = fmt.Sprintf("%s:%d", runtime.FuncForPC(pc).Name(), lineno)
	}

	msg := format
	if len(args) > 0 {
		msg = fmt.Sprintf(format, args...)
	}

	// Make the log record
	rec := &LogRecord{
		Level:   lvl,
		Created: time.Now(),
		Source:  src,
		Message: msg,
	}

	// Dispatch the logs
	for _, filt := range log {
		if lvl < filt.Level {
			continue
		}
		filt.LogWrite(rec)
	}
}

// Send a closure log message internally
func (log Logger) intLogc(lvl Level, closure func() string) {
	skip := true

	// Determine if any logging will be done
	for _, filt := range log {
		if lvl >= filt.Level {
			skip = false
			break
		}
	}
	if skip {
		return
	}

	// Determine caller func
	pc, _, lineno, ok := runtime.Caller(2)
	src := ""
	if ok {
		src = fmt.Sprintf("%s:%d", runtime.FuncForPC(pc).Name(), lineno)
	}

	// Make the log record
	rec := &LogRecord{
		Level:   lvl,
		Created: time.Now(),
		Source:  src,
		Message: closure(),
	}

	// Dispatch the logs
	for _, filt := range log {
		if lvl < filt.Level {
			continue
		}
		filt.LogWrite(rec)
	}
}

// Send a log message with manual level, source, and message.
func (log Logger) Log(lvl Level, source, message string) {
	skip := true

	// Determine if any logging will be done
	for _, filt := range log {
		if lvl >= filt.Level {
			skip = false
			break
		}
	}
	if skip {
		return
	}

	// Make the log record
	rec := &LogRecord{
		Level:   lvl,
		Created: time.Now(),
		Source:  source,
		Message: message,
	}

	// Dispatch the logs
	for _, filt := range log {
		if lvl < filt.Level {
			continue
		}
		filt.LogWrite(rec)
	}
}

// Logf logs a formatted log message at the given log level, using the caller as
// its source.
func (log Logger) Logf(lvl Level, format string, args ...interface{}) {
	log.intLogf(lvl, format, args...)
}

// Logc logs a string returned by the closure at the given log level, using the caller as
// its source.  If no log message would be written, the closure is never called.
func (log Logger) Logc(lvl Level, closure func() string) {
	log.intLogc(lvl, closure)
}

// Finest logs a message at the finest log level.
// See Debug for an explanation of the arguments.
func (log Logger) Finest(arg0 interface{}, args ...interface{}) {
	const (
		lvl = FINEST
	)
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(lvl, first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(lvl, first)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(lvl, fmt.Sprint(arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Fine logs a message at the fine log level.
// See Debug for an explanation of the arguments.
func (log Logger) Fine(arg0 interface{}, args ...interface{}) {
	const (
		lvl = FINE
	)
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(lvl, first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(lvl, first)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(lvl, fmt.Sprint(arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Debug is a utility method for debug log messages.
// The behavior of Debug depends on the first argument:
// - arg0 is a string
//   When given a string as the first argument, this behaves like Logf but with
//   the DEBUG log level: the first argument is interpreted as a format for the
//   latter arguments.
// - arg0 is a func()string
//   When given a closure of type func()string, this logs the string returned by
//   the closure iff it will be logged.  The closure runs at most one time.
// - arg0 is interface{}
//   When given anything else, the log message will be each of the arguments
//   formatted with %v and separated by spaces (ala Sprint).
func (log Logger) Debug(arg0 interface{}, args ...interface{}) {
	const (
		lvl = DEBUG
	)
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(lvl, first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(lvl, first)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(lvl, fmt.Sprint(arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Trace logs a message at the trace log level.
// See Debug for an explanation of the arguments.
func (log Logger) Trace(arg0 interface{}, args ...interface{}) {
	const (
		lvl = TRACE
	)
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(lvl, first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(lvl, first)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(lvl, fmt.Sprint(arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Info logs a message at the info log level.
// See Debug for an explanation of the arguments.
func (log Logger) Info(arg0 interface{}, args ...interface{}) {
	const (
		lvl = INFO
	)
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(lvl, first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(lvl, first)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(lvl, fmt.Sprint(arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Warn logs a message at the warning log level and returns the formatted error.
// At the warning level and higher, there is no performance benefit if the
// message is not actually logged, because all formats are processed and all
// closures are executed to format the error message.
// See Debug for further explanation of the arguments.
func (log Logger) Warn(arg0 interface{}, args ...interface{}) {
	const (
		lvl = WARNING
	)
	var msg string
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		msg = fmt.Sprintf(first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		msg = first()
	default:
		// Build a format string so that it will be similar to Sprint
		msg = fmt.Sprintf(fmt.Sprint(first)+strings.Repeat(" %v", len(args)), args...)
	}
	log.intLogf(lvl, msg)
}

// Error logs a message at the error log level and returns the formatted error,
// See Warn for an explanation of the performance and Debug for an explanation
// of the parameters.
func (log Logger) Error(arg0 interface{}, args ...interface{}) {
	const (
		lvl = ERROR
	)
	var msg string
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		msg = fmt.Sprintf(first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		msg = first()
	default:
		// Build a format string so that it will be similar to Sprint
		msg = fmt.Sprintf(fmt.Sprint(first)+strings.Repeat(" %v", len(args)), args...)
	}
	log.intLogf(lvl, msg)
}

// Critical logs a message at the critical log level and returns the formatted error,
// See Warn for an explanation of the performance and Debug for an explanation
// of the parameters.
func (log Logger) Critical(arg0 interface{}, args ...interface{}) {
	const (
		lvl = CRITICAL
	)
	var msg string
	switch first := arg0.(type) {
	case string:
		// Use the string as a format string
		msg = fmt.Sprintf(first, args...)
	case func() string:
		// Log the closure (no other arguments used)
		msg = first()
	default:
		// Build a format string so that it will be similar to Sprint
		msg = fmt.Sprintf(fmt.Sprint(first)+strings.Repeat(" %v", len(args)), args...)
	}
	log.intLogf(lvl, msg)
}

var FormatDev = "[%D %T] [%L] [%s] %M"
var FormatDefault = "[%D %T] [%L] %M"
var FormatJSON = `{'date':'%D','time':'%T','level':'%L','line':'%s','msg':'%X'}` // single object on each line, msg escaped
var DefaultStderrLoggerName = "stderr"                                           // :)

func New(fmt string) Logger {
	if fmt == "" {
		fmt = FormatDefault
	}
	if !strings.Contains(fmt, "%") {
		panic("usage: log4go.New(formatString), format string has no verbs")
	}
	lgr := make(Logger)
	lgr.AddFilter(DefaultStderrLoggerName, TRACE, NewConsoleWriter(fmt))
	return lgr
}

func NewModule(category string, level Level, fmt string) {

}

func SetCategoryAll(l Logger, cat string) {
	for _, filter := range l {
		filter.Category = cat
	}
}
func SetLevelAll(l Logger, level Level) {
	for _, filter := range l {
		filter.Level = level
	}
}

func SetLevelAllString(l Logger, levelString string) {
	for _, filter := range l {
		filter.Level = String2Level(levelString)
	}
}
func SetFormatAll(l Logger, f string) {
	for _, filter := range l {
		lw := filter.LogWriter
		switch lw.(type) {
		case *FileLogWriter:
			lw.(*FileLogWriter).SetFormat(f)
		case *ConsoleLogWriter:
			lw.(*ConsoleLogWriter).SetFormat(f)
		default:
			panic(fmt.Sprintf("wtf is this: %T", filter))
		}

	}
}

func NewStderrFileCombo(filename, fmt string) Logger {
	m := New(fmt)
	m.AddFilter("file", TRACE, NewFileWriter(filename, fmt))
	return m
}

var Rotate, RotateDaily = false, false

func NewFileWriter(filename string, fmt string) *FileLogWriter { // LogWriter
	//lgr := NewFileLogWriter(filename, Rotate, RotateDaily) // rotate,  daily
	lgr := NewFileLogWriter(filename, Rotate, RotateDaily) // rotate,  daily
	lgr.SetFormat(fmt)
	return lgr
}

func NewConsoleWriter(fmt string) *ConsoleLogWriter { // LogWriter
	lgr := NewConsoleLogWriter()
	lgr.SetFormat(fmt)
	return lgr
}

func String2Level(levelString string) Level {
	var lvl Level = UNKNOWN
	switch strings.TrimSpace(strings.ToUpper(levelString)) {
	case "FINEST":
		lvl = FINEST
	case "FINE":
		lvl = FINE
	case "DEBUG", "DEBG":
		lvl = DEBUG
	case "TRACE", "TRAC":
		lvl = TRACE
	case "INFO":
		lvl = INFO
	case "WARNING", "WARN":
		lvl = WARNING
	case "ERROR", "ERR":
		lvl = ERROR
	case "CRITICAL", "CRIT":
		lvl = CRITICAL
	}
	return lvl
}

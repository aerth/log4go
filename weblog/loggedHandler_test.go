package weblog

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/aerth/log4go"
)

// test lo4go.Logger is a SupportedLogger...
var _ SupportedLogger = log4go.New("")

func smallTester(w http.ResponseWriter, r *http.Request) {
	// this intentionally left blank
}

func TestLoggedHandler(t *testing.T) {
	var logger SupportedLogger
	logger = log4go.New("")
	ts := httptest.NewServer(LoggedHandlerFunc(logger, smallTester))
	_, err := http.Get(ts.URL)
	if err != nil {
		panic(err)
	}
	logger.Close()
}
func TestLoggedHandlerSecondContext(t *testing.T) {
	logger := log4go.New("")
	muxrouter := mux.NewRouter()
	dummymuxHandler := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		test, ok := vars["test"]
		if !ok {
			t.Errorf("not ok")
			return
		}
		if test != "foo" {
			panic("not foo")
		}
		t.Logf("Got test: %q", test)
		fmt.Fprintf(w, "test=%v", test)
	}
	muxrouter.HandleFunc("/test/{test}", dummymuxHandler)
	ts := httptest.NewServer(LoggedHandler(logger, muxrouter))
	_, err := http.Get(ts.URL + "/test/foo")
	if err != nil {
		panic(err)
	}
	logger.Close()
}
func TestLoggedHandlerSecondContext2(t *testing.T) {
	logger := log4go.New("")
	muxrouter := mux.NewRouter()
	dummymuxHandler := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		test, ok := vars["test"]
		if !ok {
			t.Errorf("not ok")
			return
		}
		if test != "foo" {
			panic("not foo")
		}
		t.Logf("Got test: %q", test)
		fmt.Fprintf(w, "test=%v", test)
	}
	muxrouter.Handle("/test/{test}", LoggedHandlerFunc(logger, dummymuxHandler))
	ts := httptest.NewServer(muxrouter)
	_, err := http.Get(ts.URL + "/test/foo")
	if err != nil {
		panic(err)
	}
	logger.Close()
}

package weblog

import (
	"context"
	"crypto/rand"
	"fmt"
	"net/http"
	"reflect"
	"runtime"
	"strings"
	"time"
)

// RequestIDlength of the generated reqid
var RequestIDlength = 4

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

type SupportedLogger interface {
	Debug(f interface{}, i ...interface{})
	Warn(f interface{}, i ...interface{})
	Info(f interface{}, i ...interface{})
	Error(f interface{}, i ...interface{})
	Trace(f interface{}, i ...interface{})
	Critical(f interface{}, i ...interface{})

	Sync()
	Close()
}

type weblog struct {
	h      http.Handler
	logger SupportedLogger
	hname  string
}

func funcname(i interface{}) string {
	if i == nil {
		return "<nil>"
	}
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}
func trace2() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return frame.Function
}
func (www weblog) ServeHTTP(w http.ResponseWriter, orig *http.Request) {
	var (
		t1    = time.Now()
		r     = SetRequestID(orig)
		reqid = REQID(r)
		lgfn  = www.logger.Debug
		lrw   = &loggingResponseWriter{w, http.StatusOK} // default is 200 if no headerset
	)
	if www.hname == "" {
		www.hname = fmt.Sprintf("%s()", funcname(www.h))
	}
	// always DEBUG level for incoming request log
	lgfn("<-- %s %s %s %s", reqid, r.Method, r.URL.Path, www.hname)

	// serve request (with /our/ writer)
	www.h.ServeHTTP(lrw, r)

	// but could be WARN level for response log
	if lrw.statusCode != 200 && lrw.statusCode != 302 { // TODO: probably some more status to keep debug
		lgfn = www.logger.Warn
	}

	// log response, path, time taken
	lgfn("--> %s %s %d %s %q (%s)", reqid, r.Method, lrw.statusCode, http.StatusText(lrw.statusCode), r.URL.Path, time.Since(t1))
}

// HTTP is a bad ass logger that gives us cool output like this:
//
//     [2020/12/28 09:04:42 UTC] [DEBUG] [logpkg.weblog.ServeHTTP:63] --> POST /login
//     [2020/12/28 09:04:42 UTC] [WARN] [logpkg.weblog.ServeHTTP:70] <-- 500 Internal Server Error "/login" (210.571µs)
func LoggedHandler(logger SupportedLogger, underlyingHandler http.Handler) http.Handler {
	return weblog{h: underlyingHandler, logger: logger}
}
func LoggedHandlerFunc(logger SupportedLogger, underlyingHandlerFn http.HandlerFunc) http.Handler {
	return weblog{h: underlyingHandlerFn, logger: logger}
}

// override WriteHeader, just saving response code
func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

const RequestID = "request-id"
const RequestLogger = "request-logger"
const RequestStarted = "request-started"

// it could return a completely new request, ignoring the uuid.New(len)
var RequestIDeffect func(string) string = strings.ToUpper

var NewUUID = func(l int) string {
	b := make([]byte, l/2)
	rand.Read(b)
	return fmt.Sprintf("%02X", b)
}

func newContext(orig context.Context, otherValues ...map[string]interface{}) context.Context {
	ctx := orig
	reqid := RequestIDeffect(NewUUID(RequestIDlength))
	ctx = context.WithValue(ctx, RequestID, reqid)
	ctx = context.WithValue(ctx, RequestStarted, time.Now())
	for _, c := range otherValues {
		for k, v := range c {
			ctx = context.WithValue(ctx, k, v)
		}
	}
	return ctx
}

func REQID(r *http.Request) string {
	ctx := r.Context()
	if ctx == nil {
		panic("ouch, request has no context.")
	}
	reqid, ok := ctx.Value(RequestID).(string)
	if !ok {
		return "[REQ UNKN ERR]"
	}
	return fmt.Sprintf("[REQ %s]", reqid)
}

func SetRequestID(r *http.Request) *http.Request {
	return r.WithContext(newContext(r.Context()))
}
